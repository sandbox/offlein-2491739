SUMMARY
-------
A graphical user interface for manipulating Data via tables using the jQWidgets
(specifically jqxGrid) library.


REQUIREMENTS
------------
 * Data module
 * jQWidgets v.3.8.0
 * jQuery 1.8+ via jQuery Update module
 * Libraries module to provide the jQWidgets library.


INSTALLATION
------------
 * Install as usual, see http://drupal.org/node/70151 for further information.
 * Ensure that you have jQuery Update installed and you're using jQuery 1.8 or
   higher.
 * Download the jQWidgets library from http://www.jqwidgets.com/download/, and
   extract it.
 * Copy the /jqwidgets directory within (which should have jqxcore.js in the
   root of it) into your Libraries path.
 * From the "Edit" page of a table, you should see a new "Manipulate Data"
   tab. The URL will be:
     http://example.com/admin/structure/data/edit/TABLE_NAME/manipulate
   where TABLE_NAME is the machine name of your table.


CONTACT
-------
Current maintainers:
 * Craig Leinoff (Offlein) - https://www.drupal.org/user/96002
