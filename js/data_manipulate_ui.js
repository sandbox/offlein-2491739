/**
 * @file
 * Data Manipulate UI client-side processing.
 */

(function ($) {
  Drupal.behaviors.data_manipulate_ui = {
    attach: function (context, settings) {
      if (Drupal.settings.data_manipulate_ui) {
        // Load data.
        var source = Drupal.settings.data_manipulate_ui.source;

        // Handle sending data binding updates.
        source.updaterow = function(rowid, newdata, commit) {
          sendToDrupal(newdata, rowid, commit);
        };
        source.deleterow = function(rowid, commit) {
          deleteFromDrupal(rowid, commit);
        };

        // Set up the dataAdapter.
        var dataAdapter = new $.jqx.dataAdapter(source);

        // Make the settings array, generated in Drupal, use the data adapter.
        Drupal.settings.data_manipulate_ui
            .jqxgrid_settings.source = dataAdapter;

        // Add a delete button to the settings array.
        Drupal.settings.data_manipulate_ui.jqxgrid_settings.columns.push({
          'text': ' ',
          'datafield': 'Delete',
          'columnType': 'button',
          'cellsrenderer': function() {
            return 'x';
          },
          'buttonclick': function(row) {
            // Open the popup window when the user clicks a button.
            id = $jqxGrid.jqxGrid('getrowid', row);
            $jqxGrid.jqxGrid('deleterow', id);
          }
        });

        // Turn on the grid.
        $jqxGrid = $("#jqxgrid");
        $jqxGrid.jqxGrid(Drupal.settings.data_manipulate_ui.jqxgrid_settings);

        // Hook up the "add" button.
        $("#jqxgrid-add-row").jqxButton({
          theme: 'arctic',
          height: 30
        });
        $("#jqxgrid-add-row").click(function() {
          var defaults = {};
          defaults[Drupal.settings.data_manipulate_ui.source.id] = '-';
          $("#jqxgrid").jqxGrid('addrow', null, defaults);
        });

        function deleteFromDrupal(rowId, commit) {
          var body = {
            'id': rowId,
            'csrf_token': Drupal.settings.data_manipulate_ui.csrf_token
          };

          var data =
              $.ajax('/ajax/data_manipulate_ui/' +
              Drupal.settings.data_manipulate_ui.table_name, {
                method: 'DELETE',
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                data: JSON.stringify(body)
              }).done(function(d, status) {
                $jqxGrid.jqxGrid('updatebounddata');
                commit(true);
              }).fail(function(data, textStatus, errorThrown) {
                var msg = "Failed to delete row - " + errorThrown;
                if (data.responseText) {
                  msg += ": " + data.responseText;
                }
                alert(msg);
                commit(false);
              });
        }
        function sendToDrupal(row, rowId, commit) {
          var method = 'PUT';

          var rowUniqueId = row.uid;
          delete row.uid;

          if (rowId == '-') {
            method = 'POST';
            rowId = '';
          }

          var body = {
            'row': row,
            'id': rowId,
            'csrf_token': Drupal.settings.data_manipulate_ui.csrf_token
          };

          var data =
              $.ajax('/ajax/data_manipulate_ui/' +
              Drupal.settings.data_manipulate_ui.table_name, {
                method: method,
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                data: JSON.stringify(body)
              }).done(function(d, status) {
                $jqxGrid.jqxGrid('updatebounddata');
                commit(true);
              }).fail(function(data, textStatus, errorThrown) {
                var msg = "Failed to save data - " + errorThrown;
                if (data.responseText) {
                  msg += ": " + data.responseText;
                }
                alert(msg);
                commit(false);
              });
        }

      }
    }
  };
}(jQuery));
