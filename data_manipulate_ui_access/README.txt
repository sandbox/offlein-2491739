SUMMARY
-------
Per-user access control for Data Manipulation tables via the Data Manipulate UI
module.


REQUIREMENTS
------------
 * data_manipulate_ui module


INSTALLATION
------------
 * Install as usual, see http://drupal.org/node/70151 for further information.
 * As a user with Data table administration privileges, edit your data table of
   choice. You should see a new "Access Control" tab. From here you may specify
   users (via username) who can access the UI for manipulating this table.


CONTACT
-------
Current maintainers:
 * Craig Leinoff (Offlein) - https://www.drupal.org/user/96002
